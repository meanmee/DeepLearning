function [ cost, grad ] = stackedAECost(theta, inputSize, hiddenSize, ...
                                              numClasses, netconfig, ...
                                              lambda, data, labels)
                                         
% stackedAECost: Takes a trained softmaxTheta and a training data set with labels,
% and returns cost and gradient using a stacked autoencoder model. Used for
% finetuning.
                                         
% theta: trained weights from the autoencoder
% visibleSize: the number of input units
% hiddenSize:  the number of hidden units *at the 2nd layer*
% numClasses:  the number of categories
% netconfig:   the network configuration of the stack
% lambda:      the weight regularization penalty
% data: Our matrix containing the training data as columns.  So, data(:,i) is the i-th training example. 
% labels: A vector containing labels, where labels(i) is the label for the
% i-th training example


%% Unroll softmaxTheta parameter

% We first extract the part which compute the softmax gradient
softmaxTheta = reshape(theta(1:hiddenSize*numClasses), numClasses, hiddenSize);

% Extract out the "stack"
stack = params2stack(theta(hiddenSize*numClasses+1:end), netconfig);

% You will need to compute the following gradients
softmaxThetaGrad = zeros(size(softmaxTheta));
stackgrad = cell(size(stack));
for delta = 1:numel(stack)
    stackgrad{delta}.w = zeros(size(stack{delta}.w));
    stackgrad{delta}.b = zeros(size(stack{delta}.b));
end

cost = 0; % You need to compute this

% You might find these variables useful
M = size(data, 2);
groundTruth = full(sparse(labels, 1:M, 1));%input labels


%% --------------------------- YOUR CODE HERE -----------------------------
%  Instructions: Compute the cost function and gradient vector for 
%                the stacked autoencoder.
%
%                You are given a stack variable which is a cell-array of
%                the weights and biases for every layer. In particular, you
%                can refer to the weights of Layer d, using stack{d}.w and
%                the biases using stack{d}.b . To get the total number of
%                layers, you can use numel(stack).
%
%                The last layer of the network is connected to the softmax
%                classification layer, softmaxTheta.
%
%                You should compute the gradients for the softmaxTheta,
%                storing that in softmaxThetaGrad. Similarly, you should
%                compute the gradients for each layer in the stack, storing
%                the gradients in stackgrad{d}.w and stackgrad{d}.b
%                Note that the size of the matrices in stackgrad should
%                match exactly that of the size of the matrices in stack.
%

% -------------------------------------------------------------------------
depth = numel(stack)% 神经网络的层数(不包括softmax层)

z = cell(depth+1,1);
a = cell(depth+1,1);%进行前馈神经网络计算所需要的参数

a{1} =data;%输入层

for index = 1:depth%前馈神经网络计算(为什么要加1)
    z{index+1} =  stack{index}.w*a{index}+repmat(stack{index}.b, 1, size(a{index},2));
    a{index+1} = sigmoid(z{index+1});
end

model = softmaxTheta*a{depth+1}; %神经网络最后一层的activation传入softmax regression。
model = bsxfun(@minus, model , max(model , [], 1));  
h = exp(model );
h =  bsxfun(@rdivide, h, sum(h));  
size(groundTruth);
cost = -1/numClasses*sum(sum(groundTruth.*log(h)))+lambda/2*sum(sum(softmaxTheta.^2));
softmaxThetaGrad = -1/numClasses*((groundTruth-h)*a{depth+1}')+lambda*softmaxTheta;

%反向传播算法
delta = cell(depth+1);
%I is the input labels and P is the vector of conditional probabilities.
delta{depth+1} = -(softmaxTheta' * (groundTruth - h)) .* a{depth+1} .* (1-a{depth+1});
for layer = (depth:-1:2)
  delta{layer} = (stack{layer}.w' * delta{layer+1}) .* a{layer} .* (1-a{layer});
end
for layer = (depth:-1:1)
  stackgrad{layer}.w = (1/numClasses) * delta{layer+1} * a{layer}';
  stackgrad{layer}.b = (1/numClasses) * sum(delta{layer+1}, 2);
end
%% Roll gradient vector
grad = [softmaxThetaGrad(:) ; stack2params(stackgrad)];

end
% You might find this useful
function sigm = sigmoid(x)
    sigm = 1 ./ (1 + exp(-x));
end
